//Folders
def workspaceFolderName = "${WORKSPACE_NAME}"
def projectFolderName = "${PROJECT_NAME}"
def sampleJobName = projectFolderName + "/SampleFolder"
def sampleFolder = folder(sampleJobName) { displayName('Folder created by Jenkins') }

//Jobs
def job1 = freeStyleJob(sampleJobName + "/FirstJob")
def job2 = freeStyleJob(sampleJobName + "/SecondJob")

//Pipeline
def samplepipeline = buildPipelineView(sampleJobName + "/Sample-Pipeline-Demo")

samplepipeline.with{
	title('Pipeline Demo')
	displayedBuilds(5)
	selectedJob(sampleJobName + "/FirstJob")
	showPipelineParameters()
	refreshFrequency(5)
}

//Job Configurations
job1.with{
	scm{
		git{
		   remote{
			url('https://pat_arvesu01@bitbucket.org/pat_arvesu01/cartridge.git')
			credentials("adop-jenkins-master")
		   }
		branch("*/master")
	 	}
	}
	steps{
	shell('''#!/bin/sh
	ls -lart''')
	}
	publishers{
	    downstreamParameterized{
		trigger(sampleJobName + "/SecondJob"){
			condition("SUCCESS")
			parameters{
				predefinedProp("CUSTOM_WORKSPACE",'$WORKSPACE')
			}
		}
	     }
        }
}
job2.with{
	parameters{
		stringParam("CUSTOM_WORKSPACE","","")
	}
	steps{
	shell('''
	#!/bin/sh
	ls -lart $CUSTOM_WORKSPACE
	''')
	}
}